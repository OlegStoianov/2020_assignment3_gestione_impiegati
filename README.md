# Assignment 3: Gestione impiegati

## Installazione

* Scaricare una copia locale del repository (es. attraverso il comando git clone)
* Da prompt dei comandi eseguire:
### Windows:
```cmd
mvnw.cmd clean package spring-boot:repackage
```
### Linux:
```cmd
./mvnw clean package spring-boot:repackage
```
* Assicurarsi di aver installato nel pc la versione [Java 15](https://www.oracle.com/java/technologies/javase-jdk15-downloads.html), quindi assicurarsi di avere le variabili d’ambiente impostate correttamente **JAVA_HOME** e **PATH**
* Da prompt dei comandi eseguire:
```cmd
java -jar target/assignment3-0.0.1-SNAPSHOT.jar
```
* L’applicazione sarà disponibile in locale all’url: http://localhost:8080/.

