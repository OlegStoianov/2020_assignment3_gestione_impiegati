package edu.unimib.psw.assignment3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.unimib.psw.assignment3.entity.Cliente;
import edu.unimib.psw.assignment3.service.ClienteService;

@Controller
@RequestMapping("/clienti/")
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@RequestMapping("list")
	public String listaClienti(Model model) {    
		List<Cliente> clienti = clienteService.getAllClienti();
		model.addAttribute("clienti", clienti);
		return "clienti";
	}

	@PostMapping("salva")
	public String salvaCliente(@ModelAttribute("cliente") Cliente cliente) {
		clienteService.saveCliente(cliente);     
		return "redirect:/clienti/list";
	}


	@RequestMapping("nuovo")
	public String nuovoCliente(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("cliente", cliente);
		return "new_cliente";
	}

	@RequestMapping("modifica/{id}")
	public String modificaCliente(@PathVariable(name = "id") int id, Model model) {
		Cliente cliente = clienteService.getCliente(id);
		model.addAttribute("cliente", cliente);
		return "edit_cliente";
	}

	@PostMapping("aggiorna/{id}")
	public String aggiornaCliente(@PathVariable("id") long id, Cliente cliente, Model model) {
		clienteService.updateCliente(id,cliente);
		model.addAttribute("clienti", clienteService.getAllClienti());
		return "redirect:/clienti/list";

	}


	@GetMapping("elimina/{id}")
	public String eliminaCliente(@PathVariable("id") long id, Model model) {
		clienteService.deleteCliente(id);
		model.addAttribute("clienti", clienteService.getAllClienti());
		return "redirect:/clienti/list";
	}



}
