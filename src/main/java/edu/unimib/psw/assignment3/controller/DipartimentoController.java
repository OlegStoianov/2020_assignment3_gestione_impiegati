package edu.unimib.psw.assignment3.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.unimib.psw.assignment3.entity.Area;
import edu.unimib.psw.assignment3.entity.Dipartimento;
import edu.unimib.psw.assignment3.service.DipartimentoService;

@Controller
@RequestMapping("/dipartimenti/")
public class DipartimentoController {

	@Autowired
	DipartimentoService dipService;

	@RequestMapping("list")
	public String listaDipartimenti(Model model) {    
		List<Dipartimento> dipartimenti = dipService.getAllDipartimento();
		model.addAttribute("dipartimenti", dipartimenti);
		return "dipartimenti";
	}

	@PostMapping("salva")
	public String salvaDipartimento(@Valid Dipartimento dipartimento, BindingResult result, Model model) {
		if(result.hasErrors()) {
			return "new_dipartimento";
		}
		dipService.saveDipartimento(dipartimento);     
		return "redirect:/dipartimenti/list";
	}


	@RequestMapping("nuovo")
	public String nuovoDipartimento(Model model) {
		Dipartimento dipartimento = new Dipartimento();
		model.addAttribute("dipartimento", dipartimento);
		model.addAttribute("aree", Area.values());
		return "new_dipartimento";
	}

	@RequestMapping("modifica/{id}")
	public String modificaDipartimento(@PathVariable(name = "id") int id, Model model) {
		Dipartimento dipartimento = dipService.getDipartimento(id);
		model.addAttribute("dipartimento", dipartimento);
		return "edit_dipartimento";
	}

	@PostMapping("aggiorna/{id}")
	public String aggiornaDipartimento(@PathVariable("id") long id,@Valid Dipartimento dipartimento, BindingResult result, 
			Model model) {
		 if (result.hasErrors()) {
	            dipartimento.setId(id);
	            return "edit_dipartimento";
	        }
		dipService.updateDipartimento(id, dipartimento);
		model.addAttribute("dipartimenti", dipService.getAllDipartimento());
		return "redirect:/dipartimenti/list";

	}


	@GetMapping("elimina/{id}")
	public String eliminaDipartimento(@PathVariable("id") long id, Model model) {
		dipService.deleteDipartimento(id);
		model.addAttribute("dipartimenti", dipService.getAllDipartimento());
		return "redirect:/dipartimenti/list";
	}

}
