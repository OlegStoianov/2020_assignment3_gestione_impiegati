package edu.unimib.psw.assignment3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.unimib.psw.assignment3.entity.Dipartimento;
import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.service.DipartimentoService;
import edu.unimib.psw.assignment3.service.ImpiegatoService;

@Controller
@RequestMapping("/impiegati")
public class ImpiegatoController {

	@Autowired
	ImpiegatoService impiegatoService;

	@Autowired
	DipartimentoService dipartimentoService;



	@RequestMapping("list")
	public String listaImpiegati(Model model) {    
		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegati", impiegati);
		return "impiegati";
	}


	@PostMapping("salva")
	public String salvaImpiegato(@ModelAttribute("impiegato") Impiegato impiegato) {
		impiegatoService.saveImpiegato(impiegato);     
		return "redirect:/impiegati/list";
	}


	@RequestMapping("nuovo")
	public String nuovoImpiegato(Model model) {
		Impiegato impiegato = new Impiegato();
		model.addAttribute("impiegato", impiegato);
		List<Dipartimento> dipartimenti = dipartimentoService.getAllDipartimento();
		model.addAttribute("dipartimenti", dipartimenti);
		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegati", impiegati);
		return "new_impiegato";
	}

	@RequestMapping("modifica/{id}")
	public String modificaImpiegato(@PathVariable(name = "id") int id, Model model) {
		Impiegato impiegato = impiegatoService.getImpiegato(id);
		model.addAttribute("impiegato", impiegato);
		List<Dipartimento> dipartimenti = dipartimentoService.getAllDipartimento();
		model.addAttribute("dipartimenti", dipartimenti);
		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		impiegati.remove(impiegato);
		model.addAttribute("impiegati", impiegati);
		return "edit_impiegato";
	}

	@PostMapping("aggiorna/{id}")
	public String aggiornaImpiegato(@PathVariable("id") long id, Impiegato impiegato, Model model) {
		impiegatoService.updateImpiegato(id,impiegato);
		model.addAttribute("impiegati", impiegatoService.getAllImpiegati());
		return "redirect:/impiegati/list";

	}


	@GetMapping("elimina/{id}")
	public String eliminaImpiegato(@PathVariable("id") long id, Model model) {
		impiegatoService.deleteImpiegato(id);
		model.addAttribute("impiegati", impiegatoService.getAllImpiegati());
		return "redirect:/impiegati/list";
	}




}
