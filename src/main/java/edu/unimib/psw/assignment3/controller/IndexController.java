package edu.unimib.psw.assignment3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.unimib.psw.assignment3.entity.Cliente;
import edu.unimib.psw.assignment3.entity.Dipartimento;
import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.entity.Progetto;
import edu.unimib.psw.assignment3.service.ClienteService;
import edu.unimib.psw.assignment3.service.DipartimentoService;
import edu.unimib.psw.assignment3.service.ImpiegatoService;
import edu.unimib.psw.assignment3.service.ProgettoService;

@Controller
@RequestMapping("/")
public class IndexController {

	@Autowired
	ClienteService clienteService;
	@Autowired
	DipartimentoService dipartimentoService;
	@Autowired
	ImpiegatoService impiegatoService;
	@Autowired
	ProgettoService progettoService;

	@RequestMapping("")
	public String listaClienti(Model model) {    
		List<Cliente> clienti = clienteService.getAllClienti();
		model.addAttribute("clienti", clienti);
		List<Dipartimento> dipartimenti = dipartimentoService.getAllDipartimento();
		model.addAttribute("dipartimenti", dipartimenti);
		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegati", impiegati);
		List<Progetto> progetti = progettoService.getAllProgetti();
		model.addAttribute("progetti", progetti);
		return "index";
	}
	
}

