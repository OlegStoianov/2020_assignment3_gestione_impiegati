package edu.unimib.psw.assignment3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.unimib.psw.assignment3.entity.Cliente;
import edu.unimib.psw.assignment3.entity.Filtro;
import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.entity.Progetto;
import edu.unimib.psw.assignment3.service.ClienteService;
import edu.unimib.psw.assignment3.service.ImpiegatoService;
import edu.unimib.psw.assignment3.service.ProgettoService;

@Controller
@RequestMapping("/progetti")
public class ProgettoController {

	@Autowired
	ProgettoService progettoService;

	@Autowired
	ImpiegatoService impiegatoService;


	@Autowired
	ClienteService clienteService;


	@RequestMapping("list")
	public String listaProgetti(Model model) {    
		List<Progetto> progetti = progettoService.getAllProgetti();
		model.addAttribute("progetti", progetti);
		List<Impiegato> impiegatiFiltro = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegatiFiltro", impiegatiFiltro);
		List<Cliente> clientiFiltro = clienteService.getAllClienti();
		model.addAttribute("clientiFiltro", clientiFiltro);
		Filtro filtro = new Filtro();
		model.addAttribute("filtro", filtro);
		return "progetti";
	}


	@PostMapping("salva")
	public String salvaProgetto(@ModelAttribute("progetto") Progetto progetto) {
		progettoService.saveProgetto(progetto);     
		return "redirect:/progetti/list";
	}


	@RequestMapping("nuovo")
	public String nuovoProgetto(Model model) {
		Progetto progetto = new Progetto();
		model.addAttribute("progetto", progetto);

		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegati", impiegati);


		List<Cliente> clienti = clienteService.getAllClienti();
		model.addAttribute("clienti", clienti);


		//		List<Progetto> progetti = progettoService.getAllProgetti();
		//		model.addAttribute("progetti", progetti);
		return "new_progetto";
	}

	@RequestMapping("modifica/{id}")
	public String modificaProgetto(@PathVariable(name = "id") int id, Model model) {
		Progetto progetto = progettoService.getProgetto(id);
		model.addAttribute("progetto", progetto);

		List<Impiegato> impiegati = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegati", impiegati);

		List<Cliente> clienti = clienteService.getAllClienti();
		model.addAttribute("clienti", clienti);

		return "edit_progetto";
	}

	@PostMapping("aggiorna/{id}")
	public String aggiornaProgetto(@PathVariable("id") long id, Progetto progetto, Model model) {
		progettoService.updateProgetto(id,progetto);
		model.addAttribute("progetti", progettoService.getAllProgetti());
		return "redirect:/progetti/list";

	}


	@GetMapping("elimina/{id}")
	public String eliminaProgetto(@PathVariable("id") long id, Model model) {
		progettoService.deleteProgetto(id);
		model.addAttribute("progetti", progettoService.getAllProgetti());
		return "redirect:/progetti/list";
	}

	@GetMapping("cerca")
	public String cercaProgetti(@ModelAttribute Filtro filtro, Model model) {
		
		List<Progetto> progetti = progettoService.listAllProgetti(filtro);
		List<Impiegato> impiegatiFiltro = impiegatoService.getAllImpiegati();
		model.addAttribute("impiegatiFiltro", impiegatiFiltro);
		List<Cliente> clientiFiltro = clienteService.getAllClienti();
		model.addAttribute("clientiFiltro", clientiFiltro);
		model.addAttribute("progetti",progetti);
		model.addAttribute("filtro", filtro);
		return "progetti";

	}













}
