package edu.unimib.psw.assignment3.entity;

public enum Area {
	INFORMATICA,
	ECONOMICA,
	RISORSE_UMANE
}
