package edu.unimib.psw.assignment3.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

@Entity
public class Cliente extends Lavoratore {

	private String nomeAzienda;
	
	
	@ManyToMany(targetEntity=Progetto.class, mappedBy="clienti", fetch=FetchType.EAGER)
	private List<Progetto> progetti = new ArrayList<Progetto>();
	
	public Cliente(String nome, String cognome, String nomeAzienda) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.nomeAzienda = nomeAzienda;
	}
	
	public Cliente() {
		
	}

	public String getNomeAzienda() {
		return nomeAzienda;
	}

	public void setNomeAzienda(String nomeAzienda) {
		this.nomeAzienda = nomeAzienda;
	}

	public List<Progetto> getProgetti() {
		return progetti;
	}

	public void setProgetti(List<Progetto> progetti) {
		this.progetti = progetti;
	}
	
	
	
}
