package edu.unimib.psw.assignment3.entity;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Dipartimento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String nomeDipartimento;
	
	private Area areaDipartimento;
	
	private String numTelefono;
	
	@OneToMany(mappedBy="dipartimento")
	@Access(AccessType.FIELD)
	private List<Impiegato> impiegati;
	
	public Dipartimento(String nomeDipartimento, Area areaDipartimento, String numTelefono) {
		this.nomeDipartimento = nomeDipartimento;
		this.areaDipartimento = areaDipartimento;
		this.numTelefono = numTelefono;
	}
	
	public Dipartimento() {
		
	}

	public String getNomeDipartimento() {
		return nomeDipartimento;
	}

	public void setNomeDipartimento(String nomeDipartimento) {
		this.nomeDipartimento = nomeDipartimento;
	}

	public Area getAreaDipartimento() {
		return areaDipartimento;
	}

	public void setAreaDipartimento(Area areaDipartimento) {
		this.areaDipartimento = areaDipartimento;
	}

	public String getNumTelefono() {
		return numTelefono;
	}

	public void setNumTelefono(String numTelefono) {
		this.numTelefono = numTelefono;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Impiegato> getImpiegati() {
		return impiegati;
	}

	public void setImpiegati(List<Impiegato> impiegati) {
		this.impiegati = impiegati;
	}
	
	
	
	
}
