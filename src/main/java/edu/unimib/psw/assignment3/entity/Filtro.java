package edu.unimib.psw.assignment3.entity;

public class Filtro {

	private String nomeProgetto;
	private String cognomeImpiegato;
	private String nomeAzienda;


	public Filtro() {

	}
	
	
	public Filtro(String nomeProgetto, String cognomeImpiegato, String nomeAzienda) {
		super();
		this.nomeProgetto = nomeProgetto;
		this.cognomeImpiegato = cognomeImpiegato;
		this.nomeAzienda = nomeAzienda;
	}


	public String getNomeProgetto() {
		return nomeProgetto;
	}


	public void setNomeProgetto(String nomeProgetto) {
		this.nomeProgetto = nomeProgetto;
	}


	public String getCognomeImpiegato() {
		return cognomeImpiegato;
	}


	public void setCognomeImpiegato(String cognomeImpiegato) {
		this.cognomeImpiegato = cognomeImpiegato;
	}


	public String getNomeAzienda() {
		return nomeAzienda;
	}


	public void setNomeAzienda(String nomeAzienda) {
		this.nomeAzienda = nomeAzienda;
	}
	
	
}