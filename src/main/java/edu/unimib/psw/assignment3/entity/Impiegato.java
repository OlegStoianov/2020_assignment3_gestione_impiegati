package edu.unimib.psw.assignment3.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Impiegato extends Lavoratore{

	private String ruolo;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataAssunzione;

	@ManyToOne
	private Impiegato capo;

	@ManyToOne
	private Dipartimento dipartimento;
	

	@ManyToMany(targetEntity=Progetto.class, mappedBy="impiegati", fetch=FetchType.EAGER)
	private List<Progetto> progetti = new ArrayList<Progetto>();
	

	public Impiegato(String nome, String cognome, String ruolo, Impiegato capo, Dipartimento dipartimento) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.ruolo = ruolo;
		this.dataAssunzione = LocalDate.now();
		this.capo = capo;
		this.dipartimento = dipartimento;
	}

	public Impiegato(String nome, String cognome, String ruolo, LocalDate dataAssunzione, Impiegato capo, Dipartimento dipartimento) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.ruolo = ruolo;
		this.dataAssunzione = dataAssunzione;
		this.capo = capo;
		this.dipartimento = dipartimento;
	}

	public Impiegato(String nome, String cognome, String ruolo, Dipartimento dipartimento) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.ruolo = ruolo;
		this.dataAssunzione = LocalDate.now();
		this.dipartimento = dipartimento;
		//this.capo = null;
	}

	public Impiegato() {

	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public LocalDate getDataAssunzione() {
		return dataAssunzione;
	}

	public void setDataAssunzione(LocalDate dataAssunzione) {
		this.dataAssunzione = dataAssunzione;
	}

	public Impiegato getCapo() {
		return capo;
	}

	public void setCapo(Impiegato capo) {
		this.capo = capo;
	}

	public Dipartimento getDipartimento() {
		return dipartimento;
	}

	public void setDipartimento(Dipartimento dipartimento) {
		this.dipartimento = dipartimento;
	}

	public List<Progetto> getProgetti() {
		return progetti;
	}

	public void setProgetti(List<Progetto> progetti) {
		this.progetti = progetti;
	}
}
