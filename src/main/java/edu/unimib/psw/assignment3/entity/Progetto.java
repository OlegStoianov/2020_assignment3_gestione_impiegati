package edu.unimib.psw.assignment3.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Progetto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String nomeProgetto;
	
	
	@ManyToMany
	@JoinTable(name = "PROGETTO_IMPIEGATI",
			joinColumns = { @JoinColumn(name = "PROGETTO_ID") },
			inverseJoinColumns = { @JoinColumn(name = "IMPIEGATO_ID") })
	private List<Impiegato> impiegati = new ArrayList<Impiegato>();

	@ManyToMany
	@JoinTable(name = "PROGETTO_CLIENTI",
			joinColumns = { @JoinColumn(name = "PROGETTO_ID") },
			inverseJoinColumns = { @JoinColumn(name = "CLIENTE_ID") })
	private List<Cliente> clienti = new ArrayList<Cliente>();



	public Progetto(String nomeProgetto, List<Impiegato> impiegati, List<Cliente> clienti) {
		this.nomeProgetto = nomeProgetto;
		this.impiegati = impiegati;
		this.clienti = clienti;
	}

	public Progetto() {

	}

	public String getNomeProgetto() {
		return nomeProgetto;
	}

	public void setNomeProgetto(String nomeProgetto) {
		this.nomeProgetto = nomeProgetto;
	}

	public List<Impiegato> getImpiegati() {
		return impiegati;
	}

	public void setImpiegati(List<Impiegato> impiegati) {
		this.impiegati = impiegati;
	}

	public List<Cliente> getClienti() {
		return clienti;
	}

	public void setClienti(List<Cliente> clienti) {
		this.clienti = clienti;
	}


	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
}
