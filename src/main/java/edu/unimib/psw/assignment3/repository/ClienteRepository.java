package edu.unimib.psw.assignment3.repository;

import org.springframework.data.repository.CrudRepository;

import edu.unimib.psw.assignment3.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente,Long> {

}
