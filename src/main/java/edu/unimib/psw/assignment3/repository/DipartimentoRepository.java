package edu.unimib.psw.assignment3.repository;

import org.springframework.data.repository.CrudRepository;

import edu.unimib.psw.assignment3.entity.Dipartimento;

public interface DipartimentoRepository extends CrudRepository<Dipartimento,Long> {

}
