package edu.unimib.psw.assignment3.repository;

import org.springframework.data.repository.CrudRepository;

import edu.unimib.psw.assignment3.entity.Impiegato;

public interface ImpiegatoRepository extends CrudRepository <Impiegato,Long> {

}
