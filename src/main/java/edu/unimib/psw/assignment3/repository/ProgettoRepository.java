package edu.unimib.psw.assignment3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.unimib.psw.assignment3.entity.Progetto;

public interface ProgettoRepository extends CrudRepository<Progetto,Long> {

	@Query(value = "SELECT * FROM PROGETTO pr JOIN PROGETTO_CLIENTI pc  JOIN PROGETTO_IMPIEGATI pi JOIN CLIENTE c JOIN IMPIEGATO i on pr.ID=pc.PROGETTO_ID and pr.ID=pi.PROGETTO_ID and pc.CLIENTE_ID=c.ID and pi.IMPIEGATO_ID=i.ID"
			+ " WHERE pr.nome_progetto LIKE %?1%"
			+ " AND c.nome_azienda LIKE %?3%"
			+ " AND i.cognome LIKE %?2%", nativeQuery = true)
	public List<Progetto> searchByAll(String nomeProgetto, String cognomeImpiegato, String nomeAzienda);
	
	
	@Query(value = "SELECT * FROM PROGETTO pr  LEFT OUTER JOIN PROGETTO_CLIENTI pc ON pr.id=pc.PROGETTO_iD "
			+ " LEFT OUTER JOIN PROGETTO_IMPIEGATI pi ON pr.id=pi.PROGETTO_ID"
			+ " LEFT OUTER JOIN IMPIEGATO i on pi.IMPIEGATO_ID=I.ID"
			+ " LEFT OUTER JOIN CLIENTE c on pc.CLIENTE_ID=c.ID"
			+ " WHERE pr.nome_progetto LIKE %?1%"
			+ " AND i.cognome LIKE %?2%", nativeQuery = true)
	public List<Progetto> searchByCognomeImpiegato(String nomeProgetto, String cognomeImpiegato);
	
	
	@Query(value = "SELECT * FROM PROGETTO pr  LEFT OUTER JOIN PROGETTO_CLIENTI pc ON pr.id=pc.PROGETTO_iD "
			+ " LEFT OUTER JOIN PROGETTO_IMPIEGATI pi ON pr.id=pi.PROGETTO_ID"
			+ " LEFT OUTER JOIN IMPIEGATO i on pi.IMPIEGATO_ID=I.ID"
			+ " LEFT OUTER JOIN CLIENTE c on pc.CLIENTE_ID=c.ID"
			+ " WHERE pr.nome_progetto LIKE %?1%"
			+ " OR c.nome_azienda LIKE %?2%", nativeQuery = true)
	public List<Progetto> searchByNomeAzienda(String nomeProgetto, String nomeAzienda);

	@Query(value = "SELECT * FROM PROGETTO"
			+ " WHERE nome_progetto LIKE %?1%", nativeQuery = true)
	public List<Progetto> searchByNomeProgetto(String nomeProgetto);


}
