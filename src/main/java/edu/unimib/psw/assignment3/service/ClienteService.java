package edu.unimib.psw.assignment3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unimib.psw.assignment3.entity.Cliente;
import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.entity.Progetto;
import edu.unimib.psw.assignment3.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepo;


	public void saveCliente(Cliente cliente) {
		clienteRepo.save(cliente);
	}


	public void updateCliente(long id, Cliente cliente) {
		clienteRepo.findById(id);
		clienteRepo.save(cliente);
	}

	public void deleteCliente(long id) {
		
		Cliente cliente = getCliente(id);
		for(Progetto progetto: cliente.getProgetti()) {
			List<Cliente> clienti = progetto.getClienti();
			clienti.remove(cliente);
			progetto.setClienti(clienti);
		}
		clienteRepo.deleteById(id);
	}


	public Cliente getCliente(long id) {
		return clienteRepo.findById(id).get();
	}


	public List<Cliente> getAllClienti() {
		List<Cliente> clienti= new ArrayList<>();
		for (Cliente c : clienteRepo.findAll()){
			clienti.add(c);
		}
		return clienti;
	}










}
