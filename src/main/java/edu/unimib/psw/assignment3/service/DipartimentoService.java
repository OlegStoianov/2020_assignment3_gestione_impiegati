package edu.unimib.psw.assignment3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unimib.psw.assignment3.entity.Cliente;
import edu.unimib.psw.assignment3.entity.Dipartimento;
import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.repository.DipartimentoRepository;

@Service
public class DipartimentoService {

	@Autowired
	private DipartimentoRepository dipartimentoRepo;
	
	
	public void saveDipartimento(Dipartimento dipartimento) {
		dipartimentoRepo.save(dipartimento);
	}
	
	
	public void updateDipartimento(long id, Dipartimento dipartimento) {
		dipartimentoRepo.findById(id);
		dipartimentoRepo.save(dipartimento);
	}
	
	public void deleteDipartimento(long id) {
		Dipartimento dip = getDipartimento(id);
		for(Impiegato i : dip.getImpiegati()) {
			i.setDipartimento(null);
		}
		dipartimentoRepo.deleteById(id);
	}
	
	
	public List<Dipartimento> getAllDipartimento() {
		List<Dipartimento> dipartimenti = new ArrayList<>();
		for (Dipartimento d : dipartimentoRepo.findAll()){
			dipartimenti.add(d);
		}
		return dipartimenti;
	}
	
	public Dipartimento getDipartimento(long id) {
		return dipartimentoRepo.findById(id).get();
	}


	
	
	
	
}
