package edu.unimib.psw.assignment3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unimib.psw.assignment3.entity.Impiegato;
import edu.unimib.psw.assignment3.entity.Progetto;
import edu.unimib.psw.assignment3.repository.ImpiegatoRepository;

@Service
public class ImpiegatoService {

	@Autowired
	private ImpiegatoRepository impiegatoRepo;


	public void saveImpiegato(Impiegato impiegato) {
		impiegatoRepo.save(impiegato);
	}

	public void updateImpiegato(long id, Impiegato impiegato) {
		impiegatoRepo.findById(id);
		impiegatoRepo.save(impiegato);
	}

	public List<Impiegato> getAllImpiegati() {
		List<Impiegato> impiegati= new ArrayList<>();
		for (Impiegato imp : impiegatoRepo.findAll()){
			impiegati.add(imp);
		}
		return impiegati;
	}

	public void deleteImpiegato(long id) {
		cancellaCapi(id);
		Impiegato impiegato = getImpiegato(id);
		for(Progetto progetto: impiegato.getProgetti()) {
			List<Impiegato> impiegati = progetto.getImpiegati();
			impiegati.remove(impiegato);
			progetto.setImpiegati(impiegati);
		}
		
		impiegatoRepo.deleteById(id);	
	}

	public Impiegato getImpiegato(long id) {
		return impiegatoRepo.findById(id).get();
	}
	
	
	public void cancellaCapi(long id) {
		for(Impiegato imp : getAllImpiegati()) {
			if(imp.getCapo() != null && imp.getCapo().getId() == id) {
				imp.setCapo(null);
			}
		}
	}

}
