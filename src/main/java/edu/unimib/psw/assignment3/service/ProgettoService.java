package edu.unimib.psw.assignment3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unimib.psw.assignment3.entity.Filtro;
import edu.unimib.psw.assignment3.entity.Progetto;
import edu.unimib.psw.assignment3.repository.ProgettoRepository;

@Service
public class ProgettoService {
	
	@Autowired
	private ProgettoRepository progettoRepo;
	
	
	public void saveProgetto(Progetto progetto) {
		progettoRepo.save(progetto);
	}
	
	public void deleteProgetto(long id) {
		
		
		
		
		progettoRepo.deleteById(id);
	}
	
	public void updateProgetto(long id, Progetto progetto) {
		progettoRepo.findById(id);
		progettoRepo.save(progetto);
	}
	
	
	public List<Progetto> getAllProgetti(){
		List<Progetto> progetti = new ArrayList<>();
		for (Progetto p : progettoRepo.findAll()){
			progetti.add(p);
		}
		return progetti;
	}
	
	 public List<Progetto> listAllProgetti(Filtro filtro){
	        if(!filtro.getCognomeImpiegato().equals("null") && !filtro.getNomeAzienda().equals("null")) {
	        	return progettoRepo.searchByAll(filtro.getNomeProgetto(), filtro.getCognomeImpiegato(), filtro.getNomeAzienda());
	        }
	        if(!filtro.getCognomeImpiegato().equals("null") && filtro.getNomeAzienda().equals("null")) {
	        	return progettoRepo.searchByCognomeImpiegato(filtro.getNomeProgetto(),filtro.getCognomeImpiegato());
	        }
	        if(filtro.getCognomeImpiegato().equals("null") && !filtro.getNomeAzienda().equals("null")) {
	        	return progettoRepo.searchByNomeAzienda(filtro.getNomeProgetto(),filtro.getNomeAzienda());
	        }
	        if(filtro.getCognomeImpiegato().equals("null") && filtro.getNomeAzienda().equals("null")) {
	        	return progettoRepo.searchByNomeProgetto(filtro.getNomeProgetto());
	        }
		 	return this.getAllProgetti();
	    }
	
	public Progetto getProgetto(long id) {
		return progettoRepo.findById(id).get();
	}

}
